--- OPCODEs ---



opcode | xout | xin | note
--- | :---: | :---: | ---
kill |  0 |  S
start |  0 |  S
choose | i |  i
givemearray | k |  kk[]S
Lau | 0 |  Skk
jump | k |  k
F2M |  i |  io
F2M |  k |  kO
eu | k |  kkkSO
eut | k |  kkkO
euc | 0 |  kkkSSkOOOOOO
subscale |  k |  Skk  | subdivide octave from Sroot in ksub step and output kdegree 
step |  k |  SikO | classic step to use scales from Sroot using iscale output kdegree NB the last parameter is kshiftroot to shift root and scale easy NBB if step = 0 then rest
step |  k |  iikO | classic step to use scales from Sroot using iscale output kdegree NB the last parameter is kshiftroot to shift root and scale easy NBB if step = 0 then rest
chromatic |  k |  Sk  | opcode to easy use chromatic tempered scale
bentab |  k |  kkk 
xchnset | 0 |  Si
xchan | i |  Si
xchan | k | Si
givemeheart | k |  kk[]
givemelungs | k |  kk[]
circle | k |  kk[]
pump | k |  kk[]  |  in heart output kvals[] every kdiv
pumparr | k[] |  kk[]
breathe | k |  kk[]
heartmurmur | k |  kk
suspire | k |  kk
pumps | S |  kS[]
breathes | S |  kS[]
circleconst | k |  kk[]
circleS | S |  kS[]
hardduckmeout | 0 |  SSkk
softduckmeout | 0 |  SSkk
followmeout | 0 |  SSkkP
followdrum | 0 |  SP
givemekick | 0 |  kkkSikk
kali | 0 |  kkkSkkk
givemednb | 0 |  kkS
eran | 0 |  kSkOOOOOO
edrum | 0 |  SkkP
e | 0 |  SkkkOOOO
hmelody | 0 |  SkkkSk[]
flingue | 0 |  SaOP
lfa | k |  kk
lfi | k |  kk
lfia | k |  kk
lfp | k |  kkk
lfpa | k |  kkk
lfse | k |  kkk
lowf | k |  kkPO
hlow | k |  kkOo
hlowa | k |  kkOo
hlowp | k |  kkkOo
howse | k |  kkkOo
peuh | k |  kkkOO
routemeout | 0 |  SSP
getmeout | 0 |  SP
xmeout | 0 |  SSi
goexp | k |  iii
golin | k |  iii
go | k |  iii	 | cosine interpolation between i1 in i2[itime] to i3
goi | k |  iii
comeforme | k |  i
fadeaway | k |  i
