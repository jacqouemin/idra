/*
	instr DEF

Sinstr	= "DEF"

idur	= p3
iamp	= p4
icps	= p5

;	INSTR

;	ENV
iatk	= .05
idurenv	= idur - iatk
idec	= idurenv * .25
isus	= .5
irel	= idurenv * .75

aenv	linseg 0, iatk, 1, idec, isus, irel, 0

a1	*= aenv
a2	*= aenv

;	routing
S1	sprintf	"%s-1", Sinstr
S2	sprintf	"%s-2", Sinstr

	chnmix a1, S1
	chnmix a2, S2


	endin
*/

