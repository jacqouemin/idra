	maxalloc "toomuchalone_instr", 1
	maxalloc "toomuchalone", 1

gktoomuchalone_dur		init 1

gitoomuchalone_morf		ftgen 0, 0, 3, -2, gisaw, gitri, gisquare
gitoomuchalone_dummy		ftgen 0, 0, gioscildur, 10, 1

gktoomuchalone_env		init 0

	instr toomuchalone

gktoomuchalone_dur		= p3
gktoomuchalone_amp		= p4
gktoomuchalone_cps		= p6

kph				portk abs(active:k("toomuchalone")), gktoomuchalone_dur
gktoomuchalone_env		table3 kph, int(abs(p5)), 1

	endin

	instr toomuchalone_instr

Sinstr		init "toomuchalone"

kport_amp	= gktoomuchalone_dur/8
kport_cps	= gktoomuchalone_dur/4
;kport_env	= gktoomuchalone_dur/4

kamp		= portk(gktoomuchalone_amp, kport_amp)
kcps		= portk(gktoomuchalone_cps, kport_cps)

kranfreq	= kamp/(gktoomuchalone_dur*8) ;.05

acar1		oscil3 1, (kcps/2)+oscil3(kcps/100, kcps/randomi:k(95, 105, kranfreq, 3), gisine), gisine
acar2		oscil3 1, (kcps/2)+oscil3(kcps/100, kcps/randomi:k(95, 105, kranfreq, 3), gisine), gisine

kmorf		table3	phasor:k(kamp/gktoomuchalone_dur), gieclassicr, 1

		ftmorf(kmorf, gitoomuchalone_morf, gitoomuchalone_dummy)

kmod		= kcps/randomi:k(35, 45, .05, 3)
kndx		= randomi:k(.25, .5, .05, 3)

ai1		foscil kamp, kcps+oscil3:k(kcps/100, kcps/randomi:k(95, 105, kranfreq, 3), gisine), acar1, kmod, kndx, gitoomuchalone_dummy
ai2		foscil kamp, kcps+oscil3:k(kcps/100, kcps/randomi:k(95, 105, kranfreq, 3), gisine), acar2, kmod, kndx, gitoomuchalone_dummy

kcf		table3	phasor:k(kamp/gktoomuchalone_dur), giclassic, 1
kres		= .95 * table3(phasor:k(kamp/gktoomuchalone_dur), gifade, 1)

af1		moogladder2 ai1, (kcps/2)+(kcps*(kcf*randomi:k(16, 24, kranfreq, 3))), kres
af2		moogladder2 ai2, (kcps/2)+(kcps*(kcf*randomi:k(16, 24, kranfreq, 3))), kres

;kenv		portk limit(abs(active:k("toomuchalone")), 0, 1), kport_env

kenv		portk abs(active:k("toomuchalone")), 35$ms

gktoomuchalone_env	*= kenv

a1		= af1+ai1
a2		= af2+af2

a1		*= gktoomuchalone_env
a2		*= gktoomuchalone_env

a1		buthp a1, 21.5
a2		buthp a2, 21.5

;		ROUTING
S1		sprintf	"%s-1", Sinstr
S2		sprintf	"%s-2", Sinstr

		chnmix a1, S1
		chnmix a2, S2

	endin
	alwayson("toomuchalone_instr")
