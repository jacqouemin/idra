	instr detuned

Sinstr	init "detuned_instr"
iarp	init p3/10
indx	init 0
imax	random 5, 25

while indx<imax do
	schedule Sinstr, random(divz(iarp, indx, 0), iarp), p3, p4-ampdb(-9), p5, p6-(divz(p6, limit(random(0, indx*5), 20, 20$k), 0))
	indx += 1
od

	turnoff

	endin



	instr detuned_instr

Sinstr		init "detuned"
idur		init p3
iamp		init p4
iftenv		init p5
icps		init p6

ipanfreq	random -.35, .35

ifn		init 0
imeth		init 6

a1	pluck $ampvar, icps + randomi:k(-ipanfreq, ipanfreq, 1/idur), icps, ifn, imeth
a2	pluck $ampvar, icps*2 + randomi:k(-ipanfreq, ipanfreq, 1/idur), icps, ifn, imeth

;	ENVELOPE
ienvvar		init idur/5

$env1
$env2

;	ROUTING
S1	sprintf	"%s-1", Sinstr
S2	sprintf	"%s-2", Sinstr

	chnmix a1, S1
	chnmix a2, S2

	endin
