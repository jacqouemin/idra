	instr juliet

Sinstr	= "juliet"

idur	= p3
icps	= p5
kcps	= icps

kamp	= p4

kpanfreq	= random:i(-.25, .25)

a1	fmbell kamp, kcps + kpanfreq, random:i(.05, .75), random:i(.05, .75), randomh:k(.005, .05, .05), random:i(3, 7) + randomh:k(-.5, .5, .05)
a2	fmbell kamp, kcps + kpanfreq, random:i(.05, .75), random:i(.05, .75), randomh:k(.005, .05, .05), random:i(3, 7) + randomh:k(-.5, .5, .05)

iatk	= .005
;idec	= idur / random:i(5, 7)
isus	random .85, 1
irel	= (idur-iatk-(idur/7)) *.35

;a1	*= mxadsr:a(iatk, idur / random:i(5, 7), 1, (irel - random:i((irel *.5), 0)))
;a2	*= mxadsr:a(iatk, idur / random:i(5, 7), 1, (irel - random:i((irel * .5), 0)))

a1	*= expseg:a(.00015, iatk, 1, idur / random:i(5, 7), isus, irel - random:i(0, (irel *.5)), .00015)
a2	*= expseg:a(.00015, iatk, 1, idur / random:i(5, 7), isus, irel - random:i(0, (irel *.5)), .00015)

;---routing
S1	sprintf	"%s-1", Sinstr
S2	sprintf	"%s-2", Sinstr

	chnmix a1, S1
	chnmix a2, S2

	endin
