	instr dmitri

Sinstr		init "dmitri"
idur		init p3
iamp		init p4
iftenv		init p5
icps		init p6

anoi		fractalnoise cosseg(iamp, idur, 0), cosseg(0, idur, 2)

asub1		resonx anoi, icps, icps/random:i(25, 10)
asub2		resonx anoi, icps, icps/random:i(25, 10)

avco1		vco2 cosseg(0, idur, iamp), icps*3/2, 10
avco2		vco2 cosseg(0, idur, iamp), icps*3/2, 10

asub1		balance asub1, anoi
asub2		balance asub2, anoi

apre1		sum asub1, avco1/5
apre2		sum asub2, avco2/5

iq		init 5

alow1, ahigh1, aband1	svfilter  apre1, cosseg(icps*4, idur, icps), iq
alow2, ahigh2, aband2	svfilter  apre2, cosseg(icps*4, idur, icps), iq

ivibfilter1	random 5, 7
ivibfilter2	init ivibfilter1/2

kviblow		cosseg ivibfilter1, idur, ivibfilter1*random:i(.95, 1.05)
kvibhigh	cosseg ivibfilter2, idur, ivibfilter2*random:i(.95, 1.05)

kvibhigh	= kvibhigh * expseg(1, idur/3, 1, idur/3, 2, idur/3, 2)
kviblow		= kviblow * expseg(1, idur/3, 1, idur/3, 2, idur/3, 2)

alow1		*= .5 + lfo(.5, kviblow)
alow2		*= .5 + lfo(.5, kviblow)

ahigh1		*= .5 + lfo(.5, kvibhigh)
ahigh2		*= .5 + lfo(.5, kvibhigh)

a1		sum alow1, ahigh1, flanger(aband1, expseg:a(giexpzero, idur, random:i(.065, .025)), random:i(.65, .85))
a2		sum alow2, ahigh2, flanger(aband2, expseg:a(giexpzero, idur, random:i(.065, .025)), random:i(.65, .85))

a1		/= 2
a2		/= 2

;		ENVELOPE
ienvvar		init idur/10

$env1
$env2

;		ROUTING
S1		sprintf	"%s-1", Sinstr
S2		sprintf	"%s-2", Sinstr

		chnmix a1, S1
		chnmix a2, S2

	endin
