	instr clickf

Sinstr		init "clickf"
idur		init p3
iamp		init p4
iftenv		init p5
icps		init p6

ain		fractalnoise iamp/2, random:i(1.75, 1.95)

#define clickf_krvt	#cosseg(idur, idur, idur/random:i(2, 12))#

ilpt		init 1/icps
klpt		= ilpt

;ain		*= envgen(idur/10, iftenv)
ain		*= cosseg(giexpzero, 15$ms, 1, (idur/2)-(15$ms), giexpzero)

a1		vcomb ain, $clickf_krvt, klpt, ilpt
a2		vcomb ain, $clickf_krvt, klpt, ilpt

a1		balance a1, ain
a2		balance a2, ain

a1		*= $ampvar
a2		*= $ampvar

;		ENVELOPE
ienvvar		init idur/100

;a1		*= envgen(idur, iftenv) 
;a2		*= envgen(idur, iftenv) 

$env1
$env2

;		ROUTING
S1		sprintf	"%s-1", Sinstr
S2		sprintf	"%s-2", Sinstr

		chnmix a1, S1
		chnmix a2, S2

	endin

