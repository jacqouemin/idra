ierr	lua_dofile "./__others/itv.lua"

opcode mtofk,k,k
        kmid xin
        kout = 440 * exp(0.0577622645 * (kmid - 69))
        xout kout
endop

	instr lualua

Sinstr	= "lualua"

idur	= p3
iamp	= p4
icps	= p5

;	INSTR
	
iArr[] fillarray 4, 2.25, 75, 50, 90
kArr[] init 3
kArr[0] = metro2(randomh:k(2, 2.5, 2), .33)
kArr[1] = 4	
kArr[2] = iamp

kpch	= lua_obj("itv", iArr, kArr)
kfq	= mtofk(kpch)

a1	= oscili(iamp, kfq)
a2	= oscili(0.3, kfq)

;	ENV
iatk	= .05
idurenv	= idur - iatk
idec	= idurenv * .25
isus	= .5
irel	= idurenv * .75

aenv	linseg 0, iatk, 1, idec, isus, irel, 0

a1	*= aenv
a2	*= aenv

;	routing
S1	sprintf	"%s-1", Sinstr
S2	sprintf	"%s-2", Sinstr

	chnmix a1, S1
	chnmix a2, S2


	endin

