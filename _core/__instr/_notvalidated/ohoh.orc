giohohft	ftgen 0, 0, 2, -2, gisquare, gisine
giohohmorf	ftgen 0, 0, gioscildur, 10, 1

	instr ohoh

Sinstr		init "ohoh"
idur		init p3
iamp		init p4
iftenv		init p5
icps		init p6

ift		init 8
ifreqv		init icps/175

ai1		vco2 $ampvar, icps, ift
ai2		vco2 $ampvar, icps+random(-ifreqv, ifreqv), ift

		ftmorf phasor:k(random(.95, 1)/idur), giohohft, giohohmorf

ivib		random 9.5*iamp, 13.5*iamp
ivar		init .05

ai1		*= .5+(oscil:a(.5, ivib, giohohmorf))
ai2		*= .5+(oscil:a(.5, ivib+random(-ivar, ivar), giohohmorf))

a1		moogladder ai1, icps*(75*$ampvar), .65+random(-.15, .15)
a2		moogladder ai2, icps*(75*$ampvar), .65+random(-.15, .15)

a1		balance a1, ai1
a2		balance a2, ai2

;		ENVELOPE
ienvvar		init idur/10

$env1
$env2

;		ROUTING
S1		sprintf	"%s-1", Sinstr
S2		sprintf	"%s-2", Sinstr

		chnmix a1, S1
		chnmix a2, S2

	endin
