	instr noia

Sinstr		init "noia"
idur		init p3
iamp		init p4
iftenv		init p5
icps		init p6

anoi		fractalnoise .25, .95

anoi		*= expseg(1, 5$ms, giexpzero, 5$ms, 1, 5$ms, giexpzero)
anoi		*= $ampvar

alin		linseg idur/100, idur, idur/95
kfb		expseg .95, idur, .995

a1		flanger anoi, alin, kfb
a2		flanger anoi, alin, kfb

a1		powershape a1, linseg(.15, idur, .95)
a2		powershape a2, linseg(.15, idur, .95)

;	ENVELOPE
ienvvar		init idur/10

$env1
$env2

;	ROUTING
S1	sprintf	"%s-1", Sinstr
S2	sprintf	"%s-2", Sinstr

	chnmix a1, S1
	chnmix a2, S2

	endin
