	instr wendi

Sinstr		init "wendi"
idur		init p3
iamp		init p4*.85
iftenv		init p5
icps		init p6

kampdist	linseg 5, idur, 0
kdurdist	init 2

kadpar		init .995 ;parameter for the kampdist distribution. Should be in the range of 0.0001 to 1
kddpar		init .75 ;parameter for the kdurdist distribution. Should be in the range of 0.0001 to 1

kvar		cosseg 1, idur, icps/15

kminfreq	= icps-kvar
kmaxfreq	= icps+kvar

;multiplier for the distribution's delta value for amplitude (1.0 is full range)
kampscl		cosseg .05, idur, 1

;multiplier for the distribution's delta value for duration
kdurscl		cosseg 0, idur, iamp

initcps		init 8+(iamp*48)
knum		cosseg initcps, idur, initcps*.75

;	instr
ai1	gendy $ampvar, kampdist, kdurdist, kadpar, kddpar, kminfreq, kmaxfreq, kampscl, kdurscl, initcps, knum
ai2	gendy $ampvar, kampdist, kdurdist, kadpar, kddpar, kminfreq, kmaxfreq, kampscl, kdurscl, initcps, knum

ai3	oscil $ampvar, icps*3/random(1.995, 2.005), gisine
ai4	oscil $ampvar, icps*3/random(1.995, 2.005), gisine

ai3	*= expseg(1, idur*.75, giexpzero)
ai4	*= expseg(1, idur*.75, giexpzero)

a1	sum ai1, ai3/2
a2	sum ai2, ai4/2

;a1	= ai3
;a2	= ai4

;	ENVELOPE
ienvvar		init idur/10

$env1
$env2

;	routing
S1	sprintf	"%s-1", Sinstr
S2	sprintf	"%s-2", Sinstr

	chnmix a1, S1
	chnmix a2, S2

	endin
