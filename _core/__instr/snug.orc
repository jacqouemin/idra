;	SNUG, a little FM

gksnugmod	init 25
gksnugndx	init 5


gksnugpan	init 0

	instr snug_instr

Sinstr	= "snug"

idur 	= p3
kamp 	= p4
icps 	= p5

//

indx	= p6
ipan	= p7

//

kcar 	= int(expseg:k(1, idur, idur*32))
kmod 	= int(expseg:k(i(gksnugmod), idur, 1))
kndx	= expseg:k(.05, idur, i(gksnugndx))

a1	foscili kamp/4, icps, kcar, kmod, kndx, gisquare
a2	foscili kamp/4, icps+randomi:k(-.05, .05, 1/idur, 2, 0), kcar, kmod+randomi:k(-.0015, .0015, 1/idur, 2, 0), kndx+randomi:k(-.05, .05, 1/idur), gisquare

;	envelope
iatk	= .005			;fix
idurenv	= idur - iatk
idec	= idurenv * 5/9		; 5 on 9
isus	= random:i(.05, .15)
irel	= idurenv * 4/9	 	; 4 on 9

;a1	*= mxadsr:a(iatk, idec, isus, irel)
;a2	*= mxadsr:a(iatk, idec, isus, irel)

a1	*= expseg:a(.00015, iatk, 1, idec, isus, irel, .00015)
a2	*= expseg:a(.00015, iatk, 1, idec, isus, irel, .00015)

a1	*= ipan
a2	*= 1-limit(ipan+random:i(-.15, .15), 0, 1)

;	routing
S1	sprintf	"%s-1", Sinstr
S2	sprintf	"%s-2", Sinstr

	chnmix a1, S1
	chnmix a2, S2

	endin

	instr snug_control

Sinstr	= "snug"

gksnugmod	= xchan:k(sprintf("%s.mod", Sinstr), 1)

gksnugndx	= xchan:k(sprintf("%s.ndx", Sinstr), 3)

gksnugpan	abs lfo(1, gkbeatf/128) + lfo(-1, gkbeatf/96)

	endin
	alwayson("snug_control")

	instr snug

indx	= i(gksnugndx)
ipan	= i(gksnugpan)

	schedule "snug_instr", 0,	p3, p4,		p5,	indx, ipan
	schedule "snug_instr", p3/3,	p3, p4*3/4,	p5*3/2,	indx, 1-ipan
	schedule "snug_instr", p3*2/3,	p3, p4/2,	p5*2,	indx, ipan
	schedule "snug_instr", p3,	p3, p4/4,	p5*3,	indx, 1-ipan

	turnoff
	endin
