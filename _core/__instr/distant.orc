maxalloc "distant", 1

	instr distant

Sinstr	= "distant"

idur	= abs(p3)
iamp	= p4
icps	= p5

itie	tival

      tigoto slur			;no reinitialisation on tied notes

aatk  line 1, idur, 0			;primary envelope

slur:
      if itie == 0 kgoto note		;no expression on first and second note

aslur linseg 0, idur*.25, 1, idur*.75, 0	;envelope for slurred note
aatk  = aatk + aslur

note:
	a1	vco2 iamp*k(aatk), icps, 0, itie
	a1	moogladder a1, 1000+aatk*3000, .75, itie

;	ROUTING
S1	sprintf	"%s-1", Sinstr
S2	sprintf	"%s-2", Sinstr

	chnmix a1, S1
	chnmix a1, S2

	endin
