gkaaronmod	init 5 ;mod parameter for aaron instr
gkaaronndx	init 25 ;index parameter for aaron instr

gkaarondetune	init 0 ;detune parameter for aaron instr

giaaronatk	init .0095

	instr aaron_1

Sinstr	= "aaron"
idur		init p3
iamp		init p4
iftenv		init p5
icps		init p6

//

indx		init i(gkaaronndx)
idetune 	init i(gkaarondetune)

//

ivibdiv		random 4, 8

aamp1	= abs(lfo:a($ampvar, cosseg(random:i(idur*.35, idur*.95)/ivibdiv, idur, random:i(idur*.75, idur*3.5)/ivibdiv)))
aamp2	= abs(lfo:a($ampvar, cosseg(random:i(idur*.35, idur*.95)/ivibdiv, idur, random:i(idur*.75, idur*3.5)/ivibdiv)))

//

kcar 	= 1
kmod 	= gkaaronmod
kndx	= expseg:k(indx, idur, .05)

kcps1	= icps + vibr(expseg(.05, idur, icps/(icps*12)), randomi:k(idur*3, idur*5, icps/(icps*12)), gisine)
kcps2	= icps + vibr(expseg(.05, idur, icps/(icps*12)), randomi:k(idur*3, idur*5, icps/(icps*12)), gisine)

a1	foscili aamp1, kcps1, kcar, kmod, kndx, gisine
a2	foscili aamp2, kcps2+randomi:k(-.05, .05, 1/idur, 2, 0), kcar, kmod+randomi:k(-.0015, .0015, 1/idur, 2, 0), kndx+randomi:k(-.05, .05, 1/idur), gisine

;	ENVELOPE
ienvvar		init idur/100

$env1
$env2

;	routing
S1	sprintf	"%s-1", Sinstr
S2	sprintf	"%s-2", Sinstr

	chnmix a1, S1
	chnmix a2, S2

	endin

	instr aaron_2

Sinstr	init "aaron"

//

irel	init .015

p3	init giaaronatk + irel
idur 	init p3
iamp 	init p4
icps 	init p6

ipanfreq	init random:i(-.95, .95)

//

a1	repluck random:i(.015, .35), iamp, icps + random:i(-ipanfreq, ipanfreq), randomh:k(.25, .95, random:i(.05, .15)), random:i(.05, .65), poscil(1, random:i(.05, .25),  gitri)
a2	repluck random:i(.015, .35), iamp, icps + random:i(-ipanfreq, ipanfreq), randomh:k(.25, .95, random:i(.05, .15)), random:i(.05, .65), poscil(1, random:i(.05, .25),  gitri)

a1	*= cosseg(0, giaaronatk, 1, irel, 0)
a2	*= cosseg(0, giaaronatk, 1, irel, 0)

;	routing
S1	sprintf	"%s-1", Sinstr
S2	sprintf	"%s-2", Sinstr

	chnmix a1, S1
	chnmix a2, S2

	endin

	instr aaron_3

Sinstr	= "aaron"

//

idur 	init p3

irel	init idur/5
iamp 	init p4
icps 	init p6

ipanfreq	= random:i(-.95, .95)

//

a1	repluck random:i(.015, .35), $ampvar, icps + random:i(-ipanfreq, ipanfreq), randomh:k(.25, .95, random:i(.05, .15)), random:i(.05, .65), poscil(1, random:i(.05, .25),  gisine)
a2	repluck random:i(.015, .35), $ampvar, icps + random:i(-ipanfreq, ipanfreq), randomh:k(.25, .95, random:i(.05, .15)), random:i(.05, .65), poscil(1, random:i(.05, .25),  gisine)

a1	*= cosseg(0, giaaronatk, 1, idur/5, 0)
a2	*= cosseg(0, giaaronatk, 1, idur/5, 0)

;	routing
S1	sprintf	"%s-1", Sinstr
S2	sprintf	"%s-2", Sinstr

	chnmix a1, S1
	chnmix a2, S2

	endin

	instr aaron_4

Sinstr	= "aaron"

//

idur 	= p3 / 3

iamp	= p4

aamp1	= abs(lfo:a(p4, cosseg(random:i(p3*.5, p3*.75)/2, p3, random:i(p3*.75, p3*3.5)/2)))
aamp2	= abs(lfo:a(p4, cosseg(random:i(p3*.5, p3*.75)/2, p3, random:i(p3*.75, p3*3.5)/2)))

icps 	= p6

//

af1	fractalnoise random:i(.05, .75), random:i(.05, .75)
af2	fractalnoise random:i(.05, .75), random:i(.05, .75)

kcps1	= icps + vibr(expseg(.05, idur, icps/(icps*12)), randomi:k(idur*3, idur*5, icps/(icps*12)), gisine)
kcps2	= icps + vibr(expseg(.05, idur, icps/(icps*12)), randomi:k(idur*3, idur*5, icps/(icps*12)), gisine)

ar1	resonx	af1, kcps1, icps/5
ar2	resonx	af1, kcps2, icps/5

a1	balance ar1, af1
a2	balance ar2, af2

a1	*= aamp1
a2	*= aamp2

a1	*= cosseg(0, giaaronatk, iamp, idur, 0)
a2	*= cosseg(0, giaaronatk, iamp, idur, 0)

;	routing
S1	sprintf	"%s-1", Sinstr
S2	sprintf	"%s-2", Sinstr

	chnmix a1, S1
	chnmix a2, S2

	endin

;---
;---
;---

	instr aaron_control

Sinstr	= "aaron"

gkaaronmod	= xchan:k(sprintf("%s.mod", Sinstr), 1)

gkaaronndx	= xchan:k(sprintf("%s.ndx", Sinstr), 3)

gkaarondetune	= xchan:k(sprintf("%s.detune", Sinstr), 0)

	endin
	alwayson("aaron_control")

	instr aaron

Sinstr	= "aaron"

indx	= i(gkaaronndx)

idetune = i(gkaarondetune)

	event "i", sprintf("%s_1", Sinstr), 0,	p3, p4, p5, p6

	event "i", sprintf("%s_2", Sinstr), 0,	p3, p4/6, p5, p6+idetune
	event "i", sprintf("%s_3", Sinstr), 0,	p3, p4/6, p5, p6+idetune

	event "i", sprintf("%s_2", Sinstr), 0,	p3, p4/3, p5, p6*2.11+idetune
	event "i", sprintf("%s_3", Sinstr), 0,	p3, p4/3, p5, p6*1.97+idetune

	event "i", sprintf("%s_4", Sinstr), 0,	p3, p4/5, p5, p6

	turnoff

	endin
