	instr haisentitoditom

/* Tom - From Iain McCurdy's TR-808.csd */

Sinstr		init "haisentitoditom"
idur		init p3
iamp		init p4
iftenv		init p5
icps		init p6

;		SINE

;		env
isatk		= .005
isrel		= idur - isatk

asinenv		transeg giexpzero, isatk, -1, 1, isrel, -15, .0015

ienv1		= .5/icps
ienv2		= idur-ienv1

#define		haisentitoditommod # expseg(random:i(5.85, 5.855), ienv1, 1, ienv2, random:i(.85, .95)) #

#define		haisentitoditomvibr # vibr(icps/55, linseg(icps/25, idur, icps/random:i(950, 1000)), gisine) #

asine1		oscili  $ampvar,	icps*$haisentitoditommod+$haisentitoditomvibr,				gisine
asine2		oscili  $ampvar*.35,	icps*$haisentitoditommod*random:i(1.95, 1.955)+$haisentitoditomvibr,	gisine
asine3		oscili  $ampvar*.125,	icps*$haisentitoditommod*random:i(2.95, 2.955)+($haisentitoditomvibr/2),	gisine
asine4		oscili  $ampvar*.115,	icps*$haisentitoditommod*random:i(5.95, 5.955)+($haisentitoditomvibr/3),	gisine
asine5		oscili  $ampvar*.05,	icps*$haisentitoditommod*random:i(6.05, 6.055)+($haisentitoditomvibr/4),	gisine

;		NOISE
inatk		= .005
inrel		= idur - inatk

anoienv		transeg giexpzero, inatk, -3, 1, inrel, -7, .0015

anoise		dust2 0.95, 7500
anoise		reson anoise, icps*2, icps/2, 1
anoise		buthp anoise, icps	
anoise		butlp anoise, icps*15
anoise		*= anoienv

;		MIX
a1		= (asine1 + asine3 + asine5*random:i(.25, .85) + anoise*random:i(.85, 1)) * $ampvar
a2		= (asine2 + asine4 + asine5*random:i(.25, .85) + anoise*random:i(.85, 1)) * $ampvar

;		ENVELOPE
ienvvar		init idur/10

$env1
$env2

;		ROUTE
S1		sprintf	"%s-1", Sinstr
S2		sprintf	"%s-2", Sinstr

		chnmix a1, S1
		chnmix a2, S2

		endin
