	instr forthedead

Sinstr		init "forthedead"
idur		init p3
iamp		init p4
iftenv		init p5
icps		init p6

ift		init gitri

ivar		init icps/10

ai1		oscil $ampvar, icps, ift
ai2		oscil $ampvar, icps+gauss(icps/100), ift

aplus1		areson ai1, icps*random:i(4.95, 5.95)/6*(iamp*(icps*16)), iamp*100
aplus2		areson ai2, icps*random:i(4.95, 5.95)/6*(iamp*(icps*16)), iamp*100

ivol		init $pp

a1		sum ai1, aplus1*ivol
a2		sum ai2, aplus2*ivol

a1		balance a1, ai1
a2		balance a2, ai2

;		ENVELOPE
ienvvar		init idur/100

$env1
$env2

;		ROUTING
S1		sprintf	"%s-1", Sinstr
S2		sprintf	"%s-2", Sinstr

		chnmix a1, S1
		chnmix a2, S2

	endin
