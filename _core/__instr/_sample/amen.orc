gSamen		init "amen/1/cw_amen01_175.wav"

giamen1	ftgen 0, 0, 0, 1, gSamen, 0, 0, 1
giamen2	ftgen 0, 0, 0, 1, gSamen, 0, 0, 2

gkamen_time	init 1
gkamen_offset	init 35
gkamen_dur	init 1
;------------------

	instr amen

Sinstr	init "amen"
idur	init p3
idiv	init p4
iamp	init p5
ix	init p6

ioffset	init i(gkamen_offset)

korgan	chnget	"heart"
aorgan	interp korgan

aph1	= ((aorgan * idiv)+random:i(0, idur/ioffset)) % 1
aph2	= ((aorgan * idiv)+random:i(0, idur/ioffset)) % 1

a1	table3 aph1, giamen1, 1
a2	table3 aph2, giamen2, 1

a1	*= cosseg(0, ix, 1, p3-(ix*2), 1, ix, 0)
a2	*= cosseg(0, ix, 1, p3-(ix*2), 1, ix, 0)

a1	*= iamp
a2	*= iamp

;---routing
S1	sprintf	"%s-1", Sinstr
S2	sprintf	"%s-2", Sinstr

	chnmix a1, S1
	chnmix a2, S2

	endin

;------------------

	opcode	amen, 0, kk

kdiv, kamp xin

Sinstr	init "amen"
imax	init 8
kndx 	init 1

if 	metro:k(gkbeatf*gkamen_time) == 1 then

	if kndx == imax+1 then
		kndx = 1
	endif

	kdur	= gkbeats*gkamen_dur

	kx	= kdur/4

	schedulek nstrnum(Sinstr)+(kndx/10), gkzero, kdur+kx, kdiv, kamp, kx
	kndx += 1
 
endif

	endop
