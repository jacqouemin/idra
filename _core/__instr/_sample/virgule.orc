gSvirgule		init "opcode/virgule.wav"

givirgule1	ftgen 0, 0, 0, 1, gSvirgule, 0, 0, 1
givirgule2	ftgen 0, 0, 0, 1, gSvirgule, 0, 0, 2

gkvirgule_time	init 1
gkvirgule_offset	init 35
gkvirgule_dur	init 1
;------------------

	instr virgule

Sinstr	init "virgule"
idur	init p3
idiv	init p4
iamp	init p5
ix	init p6

ioffset	init i(gkvirgule_offset)

korgan	chnget	"heart"
aorgan	interp korgan

aph1	= ((aorgan * idiv)+random:i(0, idur/ioffset)) % 1
aph2	= ((aorgan * idiv)+random:i(0, idur/ioffset)) % 1

a1	table3 aph1, givirgule1, 1
a2	table3 aph2, givirgule2, 1

a1	*= cosseg(0, ix, 1, p3-(ix*2), 1, ix, 0)
a2	*= cosseg(0, ix, 1, p3-(ix*2), 1, ix, 0)

a1	*= iamp
a2	*= iamp

;---routing
S1	sprintf	"%s-1", Sinstr
S2	sprintf	"%s-2", Sinstr

	chnmix a1, S1
	chnmix a2, S2

	endin

;------------------

	opcode	virgule, 0, kk

kdiv, kamp xin

Sinstr	init "virgule"
imax	init 8
kndx 	init 1

if 	metro:k(gkbeatf*gkvirgule_time) == 1 then

	if kndx == imax+1 then
		kndx = 1
	endif

	kdur	= gkbeats*gkvirgule_dur

	kx	= kdur/4

	schedulek nstrnum(Sinstr)+(kndx/10), gkzero, kdur+kx, kdiv, kamp, kx
	kndx += 1
 
endif

	endop
