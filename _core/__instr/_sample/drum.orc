	instr drum

Sinstr	= "drum"

Sfile strget p4

idur	= p7

p3	= (filelen(Sfile) - .05) * idur

kpitch	= 1 + random:i(-p5, p5)
kamp	= limit(p6 + random:i(-$pp, $pp), 0, 1)

ichnls = filenchnls(Sfile)

if (ichnls == 1) then
	a1 diskin Sfile, kpitch
	a2 = a1

elseif (ichnls == 2) then
	a1, a2 diskin2 Sfile, kpitch
endif

a1	*= kamp * linseg:a(1, p3 - .005, 1, .005, 0)
a2	*= kamp * linseg:a(1, p3 - .005, 1, .005, 0)

;---routing
S1	sprintf	"%s-1", Sinstr
S2	sprintf	"%s-2", Sinstr

	chnmix a1, S1
	chnmix a2, S2

	endin
