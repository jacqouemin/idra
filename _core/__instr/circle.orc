	instr circle 

Sinstr	= "circle"

idur	= p3
Sname	= p5
iamp	= p4

Sfile		sprintf "%s%s", "circle/", "asleep-0_2.aif"
ifilelen	= filelen(Sfile) * idur

print ifilelen

iramp	init .005

aenv	linseg 0, iramp, iamp, ifilelen-(2*iramp), iamp, iramp, 0

;	INSTR
ift	ftgen 0, 0, 0, 1, Sfile, 0, 0, 0
a1, a2	loscil aenv, 1/idur, ift, 1

p3	= ifilelen

;	ROUTING
S1	sprintf	"%s-1", Sinstr
S2	sprintf	"%s-2", Sinstr

	chnmix a1, S1
	chnmix a2, S2

	endin
