	opcode	flingue, 0, SkVP
Sin, kdel, kfb, kgain xin

kdel	limit kdel, gizero, 35

Sin1	sprintf	"%s-1", Sin
Sin2	sprintf	"%s-2", Sin

ain1	chnget Sin1
ain2	chnget Sin2

;	instrument
a1	flanger ain1, a(kdel)/1000, kfb
a2	flanger ain2, a(kdel+(kdel/5))/1000, kfb

a1	*= portk(kgain, 5$ms)
a2	*= portk(kgain, 5$ms)

	chnmix a1, "mouth-1"
	chnmix a2, "mouth-2"

	endop
