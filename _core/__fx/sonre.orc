	instr sonre

;	variables
Sinstr	= "sonre"

S1	sprintf	"%s-1", Sinstr
S2	sprintf	"%s-2", Sinstr

;	routing
a1	chnget S1
a2	chnget S2

;	params
Sp1	= sprintf("%s.freq", Sinstr)
Sp2	= sprintf("%s.bw", Sinstr)

kcf	= xchan:k(Sp1, 300)
kbw	= xchan:k(Sp2, 1)

kbw	*= 75

;	instrument
af1	resonr a1, limit(kcf + random(-5, 5), 20, 20000), kbw
af2	resonr a2, limit(kcf + random(-5, 5), 20, 20000), kbw

aout1	balance	af1, a1
aout2	balance	af2, a2

;	out
	chnmix aout1, "mouth-1"
	chnmix aout2, "mouth-2"

;	clearchns
	chnclear S1
	chnclear S2

	endin

;	alwayson
	alwayson("sonre")
