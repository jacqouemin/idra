	opcode	flingue3, 0, SkVP
Sin, kdel, kfb, kgain xin

Sin1	sprintf	"%s-1", Sin
Sin2	sprintf	"%s-2", Sin

ain1	chnget Sin1
ain2	chnget Sin2

imaxfb	init .995

a1	flanger ain1, a(kdel)/1000,		kfb%imaxfb
a2	flanger ain2, a(kdel+(kdel/5))/1000,	kfb%imaxfb

kdel	*= 2
kfb	*= 2
a3	flanger ain1, a(kdel)/1000,		kfb%imaxfb
a4	flanger ain2, a(kdel+(kdel/5))/1000,	kfb%imaxfb

kdel	*= 3
kfb	*= 3
a5	flanger ain1, a(kdel)/1000,		kfb%imaxfb
a6	flanger ain2, a(kdel+(kdel/5))/1000,	kfb%imaxfb

a1	sum a1, a3, a5
a2	sum a2, a4, a6

a1	balance a1, ain1
a2	balance a2, ain2

a1	*= portk(kgain, 5$ms)
a2	*= portk(kgain, 5$ms)

	chnmix a1, "mouth-1"
	chnmix a2, "mouth-2"

	endop
