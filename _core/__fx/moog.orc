	opcode	moog, 0, SJJP
Sin, kcf, kq, kgain xin

;	INIT
if	kcf == -1 then
	kcf = 500
endif

if	kq == -1 then
	kq = .25
endif

;	LIMIT
kcf	limit kcf, 20, 20$k

;	OUTs
Sin1	sprintf	"%s-1", Sin
Sin2	sprintf	"%s-2", Sin

ain1	chnget Sin1
ain2	chnget Sin2

;	FX
a1	moogladder2 ain1, kcf, kq
a2	moogladder2 ain1, kcf, kq

a1	balance a1, ain1
a2	balance a2, ain2

a1	*= portk(kgain, 5$ms)
a2	*= portk(kgain, 5$ms)

	chnmix a1, "mouth-1"
	chnmix a2, "mouth-2"

	endop
