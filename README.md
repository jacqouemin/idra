# Introduction

HI, I AM JACQUES. I LIKE ICE CREAM AND EXTRATONE.
THIS REPO TAKES INSPIRATION FROM STEVEN YI'S WORK ON LIVE CODING IN CSOUND.

https://github.com/kunstmusik/csound-live-code

# Why "Idra"?

Names are important as we will see during all our wonderful and amazing sexions (yep, I changed "session" in "sex-ion"), so I decided to name this project as the beautiful nine-heads monster in Hercules second labour. A name inspired by FRX's images. Thanks FRX. 

It symbolises perfectly the multitude faces of this project (i.e. musical, visual, instrumental, performance, dance, sexual - ok, now I stop with this joke).

What is live coding? It is like drawing a line in the space as a constellation is: make choices between stars in the space.

# Notes (do not bother please, I am in quarantine and I need to write and control madness)

Live coding is future. It is like a poetry and music improvisation - I need to figure out where to put the body (as a good old sad traditional fellow). The body become the structure - with this method I also have a way to name and remember variables and opcodes.
So now I have everything: poetry, music, body, soul. It will be a long journey and I just put on my backpack.
Beginning with a reflection on time: time and tempo are two things. In live coding once you are inside you are in a tempo in the time, it is like be a drop in the ocean (if you did not get it, I like ocean, diamond and sometimes pppussy, too).

# SYNTAX

No majuscules please. Majuscules are for losers and people who think that there are letter better than others. All letters are equals.

Nouns and verbs with less of 5-6 letters are privileged - live coding needs speed. You are in a flux of consciousness. It is like writing Joyce's Ulysses every session. You become part of the music, not a simple instrument. You master everything with (almost) no limits. Your musical thoughts must be as quick as possible and pass thought like a wonderful creek in the mountains with birds singing around.

The idea is having two structures:
- rhythmic: showed by the 'if, endif' structure

	e.g.
	if ('rhythmic pattern opcode') == 1) then
	
	​	'opcode(
	
	​	)
	endif

where inside there is: 
- instruments:  at the moment the main instrument is called e() and it has a defined and clear structure:

	opcode e, 0, SkkkPPPP
	Sinstr, kdur, kamp, kcps1, kcps2, kcps3, kcps4, kcps5 xin
so parameters must be written in the right order (once u get used, it won't be so difficult and it has some "logic"):

1. __instrument__ used, (e.g. "puck")
2. __duration__, of the note including release (e.g. tipically if you want to use a quarter note: _gkbeats_)
3. __amplitude__, form 0 to 1 (u can use MACROs, e.g. _$f_)
4. __frequency__, (e.g. tipically use l'opcode "step(Sroot, kscale[], kdegree)" - look further)
5. (OPTIONAL) __frequency__
6. (OPTIONAL) __frequency__
7. (OPTIONAL) __frequency__

# VOCABULARY (really beta, just a bunch of ideas)

- character:		general
				- MACROs
				- Routing
- clothing:		effects
- mind:			instruments
- body:			scales
- hands:		opcodes
- communication:	OSC
- zenith:		last instruments

# GLOBAL VARIABLEs
__gkzero__ = ksmps / sr 

# ORGANs
There are 2 main tempo that are imagined from the name of 2 organs:
## heart

- __gkpulse__ (in _bpm_, tempo for heart - e.g. __gkpulse__ = 120)
- __gkbeatf__ (in _Hz_, frequency for a quarter note - e.g. at __gkpulse__ = 120, __gkbeatf__ is 2 _Hz_)
- __gkbeats__ (in _seconds_, second for a quarter note - e.g. at __gkpulse__ = 120, __gkbeats__ is .5 _sec_)
- __gkbeatn__ (number of completed cycle from the beginning)

Opcodes:
- pump()
- heartmurmur()



## lungs

- __gkbreath__ (in _bpm_, tempo for lungs)
- __gkblowf__ (in _Hz_, frequency for a quarter note)
- __gkblows__ (in _seconds_, second for a quarter note)
- __gkblown__ (number of completed cycle from the beginning)

Opcodes:
- breathe()
- suspire()

# DYNAMICs
- $fff	= 0.708;
- $ff	= 0.447;
- $f	= 0.224;
- $mf	= 0.141;
- $mp	= 0.089;
- $p	= 0.045;
- $pp	= 0.022;
- $ppp	= 0.011;

# MACROs
- $k	= - 1000 (e.g. 20$k = 20000)

# INSTRUMENTs
Instruments name are found in Shakespeare's characters, personal imagination, quotes, Spongebob.. I will keep a list of ideas in another .md file.

## Main instruments structure
The order of information is fundamental, so in almost all instruments:
- p4 = amp
- p5 = cps

I changed Steven Yi's order because in this manner I can use the opcode function of add facoltative k to create chords.

The main opcode to call an instrument in a pattern follow this order:
- S = "instr"
- k = kdur (duration of the note, release included)
- k = kamp
- k = kcps

If there are other k (until 5 - a pentachord) then another instrument is called with the same parameters.

So in the instrument the schema is:

- p3 = kdur
- p4 = kamp
- p5 = kcps

# LFOs
Update is necessary

# FREQUENCIEs
Notes are notes. Notes are frequencies.
How does the step() work?

step() recive: step(S, i[], k)
- S	= a string formatted as the ntom opcode wants:
	- 1st number is the octave
	- 2nd name of the Anglo-Saxon letter for the note
e.g.
"4C" is the central Do

- i[]	= is the scale, there are a tons of scale already inside your body. It is only necessary to write his name

e.g.
"gkdorian" is the dorian mode

- k	= the last number is the scale degree. Here comes the hard part: convention and ratio wants that if I write 1 means the 1st degree and go on. So the idea is to write an opcode that receive 1 as 1st degree and e.g. -1 for the 7th degree one octave down. That's not easy.

