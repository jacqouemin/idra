puts "Drag an ORC:"
ORC = gets.strip

puts ORC

text = File.read(ORC.to_s)
first_time = text.match(/[\d.]+/).to_s.to_f

name = ORC.match(/.*(?=\.)/)

$out = String.new

File.readlines(ORC).each do |line|

  float_time  = line.match(/[\d.]+/).to_s.to_f
  zero_time   = float_time - first_time

  
  res       = line.gsub(float_time.to_s, zero_time.round(6).to_s)
  $out      << res
  
end

File.open(name.to_s + "_ZEROTIME" + ".orc", "w") { |file| file.write($out) }
